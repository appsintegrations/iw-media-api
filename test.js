const fs = require('fs')
const path = require('path')

const { MediaApi } = require('./dist')

const cchd = new MediaApi({ accountId: 'dev', password: '123123', userId: '$pin' })

cchd
	.editImage(
		'00000000/00000000/00000000/01010002/50442A28/1455A0AA/446A21C2/F70AC677',
		fs.createReadStream(path.resolve('./test.png'))
	)
	.then(x => {
		console.log({ x })
	})
