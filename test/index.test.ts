import { MediaApi } from '../src'

// TODO: Stub API responses so that we don't actually make an API request for every time we test.

describe('MediaAPI', () => {
	const cchd = new MediaApi({
		accountId: 'dev',
		userId: '$pin',
		password: '123123',
		timeout: 30
	})

	it('adds Images', async () => {
		expect.assertions(1)

		try {
			cchd
				.addImage('./index.test.ts', 'steve - my test name')
				.then(x => {
					console.log(x)
				})
				.catch(e => {
					console.log('e2', e)
				})
		} catch (e) {
			console.log(e)
		}
	})
	it('edits Images', async () => {})
	it('adds Videos', async () => {})
	it('edits Videos', async () => {})
	it('adds HTML', async () => {})
	it('edits HTML', async () => {})
	it('adds Remote HTML', async () => {})
	it('edits Remote HTML', async () => {})
})
