import { FolderProperties } from './types/Folder'
import {
	FOLDER_ID,
	FOLDER_NAME,
	FOLDER_TYPE,
	FOLDER_TAGS,
	FOLDER_SHARED,
	SHARED_TRUE
} from './constants/Folder'

export function generateId() {
	//TODO: WTF IS GOING ON HERE?
	// $datetime = \DateTime::createFromFormat( 'U.u', sprintf( '%.f', microtime( true ) ) );
	// $now = array(
	//   'milliseconds' => round( $datetime->format( 'u' ) / 1000 ),
	//   'seconds'      => $datetime->format( 's' ),
	//   'minutes'      => $datetime->format( 'i' ),
	//   'hours'        => $datetime->format( 'G' ),
	//   'date'         => $datetime->format( 'j' ),
	//   'month'        => $datetime->format( 'n' ) - 1,
	//   'year'         => $datetime->format( 'y' )
	// );
	// $folder_id = number_format( floor(
	//   1111000000000000
	//   + ( (
	//     $now['milliseconds']
	//     + ( $now['seconds'] * 1000 )
	//     + ( $now['minutes'] * 1000 * 60 )
	//     + ( $now['hours']   * 1000 * 60 * 60 )
	//     + ( $now['date']    * 1000 * 60 * 60 * 24 )
	//     + ( $now['month']   * 1000 * 60 * 60 * 24 * 31 )
	//     + ( $now['year']    * 1000 * 60 * 60 * 24 * 31 * 12 )
	//   ) / 10 ) % 1000000000000
	// ), 0, '.', '' );
	// return $folder_id;
}

export class Folder {
	properties: FolderProperties

	constructor(properties: FolderProperties) {
		this.properties = Object.assign(
			{
				[FOLDER_ID]: null,
				[FOLDER_NAME]: null,
				[FOLDER_TYPE]: null,
				[FOLDER_TAGS]: null,
				[FOLDER_SHARED]: null
			},
			properties
		)
	}

	public getId(): number {
		return this.properties[FOLDER_ID]
	}

	public getName(): string {
		return this.properties[FOLDER_NAME]
	}

	public getTags(): string {
		return this.properties[FOLDER_TAGS]
	}

	public getType(): string {
		return this.properties[FOLDER_TYPE]
	}

	public isShared(): boolean {
		return this.properties[FOLDER_SHARED] === SHARED_TRUE
	}
}
