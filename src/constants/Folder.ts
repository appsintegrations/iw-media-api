// Folder types
export const TYPE_CAMPAIGNS = 'campaigns'
export const TYPE_CHANNELS = 'channels'
export const TYPE_FLICKS = 'flicks'
export const TYPE_MEDIA = 'media'
export const TYPE_TEMPLATES = 'templates'

// Shared values
export const SHARED_TRUE = 'true'
export const SHARED_FALSE = 'false'

export const FOLDER_ID = 'id'
export const FOLDER_NAME = 'name'
export const FOLDER_TYPE = 'type'
export const FOLDER_TAGS = 'tags'
export const FOLDER_SHARED = 'shared'
