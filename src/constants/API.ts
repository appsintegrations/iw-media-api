// Operations
export const OP_ADD = 'add'
export const OP_ADD_REMOTE = 'addRemote'
export const OP_DELETE = 'delete'
export const OP_DELETE_REMOTE = 'deleteRemote'
export const OP_EDIT = 'edit'
export const OP_EDIT_REMOTE = 'editRemote'
export const OP_LIST = 'list'
export const OP_LIST_FOLDERS = 'folderList'
export const OP_LIST_REMOTE = 'listRemote'
export const OP_VIEW = 'view'
export const OP_VIEW_REMOTE = 'viewRemote'

// Response statuses
export const STATUS_ERROR = 'ERR'
export const STATUS_OK = 'OK'

// Parameters
export const PARAM_ACTIVE = 'active'

export const PARAM_APP_FLAG = 'appFlag'
export const PARAM_APPROVED = 'approved'
export const PARAM_ATTRIBUTES = 'attribute'
export const PARAM_BACKGROUND_IMAGE = 'backgroundimage'
export const PARAM_DESCRIPTION_NOTES = 'descriptionNotes'
export const PARAM_DEVICE_CACHE = 'deviceCache'
export const PARAM_DEVICE_CACHE_INTERVAL = 'deviceCacheInterval'
export const PARAM_FLIPPIN = 'flippin'
export const PARAM_FOLDER_ID = 'folderID'
export const PARAM_GROUP_OWNER = 'groupowner'
export const PARAM_HIDDEN = 'hidden'
export const PARAM_HORIZONTAL_OFFSET = 'horizontalOffset'
export const PARAM_ID = 'id'
export const PARAM_LOGO_IMAGE = 'logoImage'
export const PARAM_MEDIA = 'media'
export const PARAM_MEDIAREF = 'mediaref'
export const PARAM_MODULE = 'module'
export const PARAM_NAME = 'name'
export const PARAM_OBJECT_ID = 'objectid'
export const PARAM_ORIENTATION = 'orientation'
export const PARAM_OWNER = 'owner'
export const PARAM_POWER_POINT = 'powerPoint'
export const PARAM_RELAY_LOCATION = 'relayLocation'
export const PARAM_RESET_ACTIVE = 'resetactive'
export const PARAM_RESET_APP_FLAG = 'resetAppFlag'
export const PARAM_RESET_DEVICE_CACHE = 'resetdevicecache'
export const PARAM_SET_ACTIVE = 'setactive'
export const PARAM_SET_APP_FLAG = 'setAppFlag'
export const PARAM_SET_DEVICE_CACHE = 'setdeviceache'
export const PARAM_TYPE = 'type'
export const PARAM_URL = 'url'
export const PARAM_USE_AS_LOGO = 'useAsLogo'
export const PARAM_USER = 'User'
export const PARAM_VERTICAL_OFFSET = 'verticalOffset'
export const PARAM_VIDEO_QUALITY = 'videoQuality'

// Configuration options
export const CONFIG_TIMEOUT = 'timeout'

export const VALID_OPERATIONS = {
	[OP_ADD]: true,
	[OP_ADD_REMOTE]: true,
	[OP_DELETE]: true,
	[OP_DELETE_REMOTE]: true,
	[OP_EDIT]: true,
	[OP_EDIT_REMOTE]: true,
	[OP_LIST]: true,
	[OP_LIST_FOLDERS]: true,
	[OP_LIST_REMOTE]: true,
	[OP_VIEW]: true,
	[OP_VIEW_REMOTE]: true,
}

export const CACHEABLE_OPERATIONS = {
	[OP_LIST]: true,
	[OP_LIST_FOLDERS]: true,
	[OP_LIST_REMOTE]: true,
}
