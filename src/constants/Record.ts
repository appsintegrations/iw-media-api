// Property names
export const ACTIVE = 'active'
export const APP_FLAG = 'appFlag'
export const APPROVED = 'approved'
export const ATTRIBUTES = 'attribute'
export const BACKGROUND_IMAGE = 'backgroundimage'
export const DATE = 'Date'
export const DESCRIPTION_NOTES = 'descriptionNotes'
export const DEVICE_CACHE_INTERVAL = 'deviceCacheInterval'
export const DURATION = 'duration'
export const FLIPPIN = 'flippin'
export const FOLDER_ID = 'folderID'
export const HIDDEN = 'hidden'
export const HIDE_MMHD = 'hideMMHD'
export const ID = 'id'
export const MEDIAREF = 'mediaref'
export const MEDIAREF_THUMB = 'mediarefThumb'
export const MEDIAREF_PREVIEW = 'mediarefPreview'
export const MODULE = 'module'
export const NAME = 'name'
export const OBJECT_ID = 'objectid'
export const ORIENTATION = 'orientation'
export const POWER_POINT = 'powerPoint'
export const RELAY_LOCATION = 'relayLocation'
export const SORT = 'sort'
export const TYPE = 'type'
export const URL = 'url'
export const USE_AS_LOGO = 'useAsLogo'
export const USE_DEVICE_CACHE = 'deviceCache'
export const USER = 'User'
export const VID_STATUS = 'vidStatus'
export const VIDEO_QUALITY = 'videoQuality'

// Types
export const TYPE_HTML = 'HTML'
export const TYPE_HTML_URL = 'HTMLURL'
export const TYPE_IMAGE = 'Image'
export const TYPE_VIDEO = 'Video'
export const TYPE_VIDEO_URL = 'videoURL'
export const TYPE_MIME_FLASH = 'application/x-shockwave-flash'
export const TYPE_MIME_JPG = 'image/jpeg'
export const TYPE_MIME_PNG = 'image/png'
export const TYPE_MIME_TIFF = 'image/tiff'

// Flag values
export const FLAG_ON = 'on'
export const FLAG_OFF = 'off'

// Orientations
export const ORIENTATION_HORIZONTAL = 'horizontal'
export const ORIENTATION_VERTICAL = 'vertical'

// Relay locations
export const RELAY_LOCATION_CCHD = 'CCHD'
export const RELAY_LOCATION_CCRS = 'CCRS'

// Video statuses
export const VID_STATUS_READY = 'Ready'

// Video qualities
export const VIDEO_QUALITY_VERY_LOW = 1
export const VIDEO_QUALITY_LOW = 2
export const VIDEO_QUALITY_MEDIUM = 3
export const VIDEO_QUALITY_HIGH = 4
export const VIDEO_QUALITY_VERY_HIGH = 5

export const EXTENSION_TYPES = {
	// images
	jpg: TYPE_IMAGE,
	jpeg: TYPE_IMAGE,
	jpe: TYPE_IMAGE,
	png: TYPE_IMAGE,
	tiff: TYPE_IMAGE,
	bmp: TYPE_IMAGE,

	// videos
	mpg: TYPE_VIDEO,
	mpeg: TYPE_VIDEO,
	mov: TYPE_VIDEO,
	flv: TYPE_VIDEO,
	mp4: TYPE_VIDEO,
	avi: TYPE_VIDEO,
	dv: TYPE_VIDEO,
	wmv: TYPE_VIDEO,

	// misc
	html: TYPE_HTML,
}

export const ALL_PROPERTIES = [
	APP_FLAG,
	ACTIVE,
	APPROVED,
	ATTRIBUTES,
	BACKGROUND_IMAGE,
	DATE,
	DESCRIPTION_NOTES,
	DEVICE_CACHE_INTERVAL,
	DURATION,
	FLIPPIN,
	FOLDER_ID,
	HIDDEN,
	HIDE_MMHD,
	ID,
	MEDIAREF,
	MEDIAREF_THUMB,
	MEDIAREF_PREVIEW,
	MODULE,
	NAME,
	OBJECT_ID,
	ORIENTATION,
	POWER_POINT,
	RELAY_LOCATION,
	SORT,
	TYPE,
	USE_AS_LOGO,
	USE_DEVICE_CACHE,
	USER,
	VID_STATUS,
	VIDEO_QUALITY,
	URL,
]
