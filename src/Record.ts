import path from 'path'

import { Properties, VideoQuality } from './types/Record'

import {
	ALL_PROPERTIES,
	APP_FLAG,
	ACTIVE,
	APPROVED,
	ATTRIBUTES,
	BACKGROUND_IMAGE,
	DATE,
	DESCRIPTION_NOTES,
	DEVICE_CACHE_INTERVAL,
	DURATION,
	FLIPPIN,
	FOLDER_ID,
	HIDDEN,
	HIDE_MMHD,
	ID,
	MEDIAREF,
	MEDIAREF_THUMB,
	MEDIAREF_PREVIEW,
	MODULE,
	NAME,
	OBJECT_ID,
	ORIENTATION,
	POWER_POINT,
	RELAY_LOCATION,
	SORT,
	TYPE,
	USE_AS_LOGO,
	USE_DEVICE_CACHE,
	USER,
	VID_STATUS,
	VIDEO_QUALITY,
	URL,
	EXTENSION_TYPES,
	FLAG_ON
} from './constants/Record'

export function getTypeForFile(file: string): string | boolean {
	const ext = path
		.extname(file)
		.toLowerCase()
		.slice(1)

	// @ts-ignore
	return EXTENSION_TYPES[ext] || false
}

export class Record {
	properties: Properties

	constructor(properties: Properties) {
		// Set all properties to a default of NULL.
		this.properties = ALL_PROPERTIES.reduce((acc, prop) => {
			//@ts-ignore
			acc[prop] = null

			return acc
		}, {})

		// Assign the given properties.
		this.properties = { ...this.properties, ...properties }
	}

	is(property: string): boolean {
		//@ts-ignore
		return this.properties[property] === FLAG_ON
	}

	public getAttributes(): string[] {
		return this.properties[ATTRIBUTES] || []
	}

	public hasAttribute(attribute: string): boolean {
		return this.getAttributes().includes(attribute)
	}

	public getDate(): Date | undefined {
		return this.properties[DATE]
	}

	public getDescriptionNotes(): string | undefined {
		return this.properties[DESCRIPTION_NOTES]
	}

	public getDeviceCacheInterval(): string | undefined {
		return this.properties[DEVICE_CACHE_INTERVAL]
	}

	public getDuration(): string | undefined {
		return this.properties[DURATION]
	}

	public getFolderId(): number | undefined {
		return this.properties[FOLDER_ID]
	}

	public getId(): string | undefined {
		return this.properties[ID]
	}

	public getMediaref(): string | undefined {
		return this.properties[MEDIAREF]
	}

	public getMediarefThumb(): string | undefined {
		return this.properties[MEDIAREF_THUMB]
	}

	public getMediarefPreview(): string | undefined {
		return this.properties[MEDIAREF_PREVIEW]
	}

	public getModule(): string | undefined {
		return this.properties[MODULE]
	}

	public getName(): string | undefined {
		return this.properties[NAME]
	}

	public getObjectId(): string | undefined {
		return this.properties[OBJECT_ID]
	}

	public getOrientation(): string | undefined {
		return this.properties[ORIENTATION]
	}

	public getRelayLocation(): string | undefined {
		return this.properties[RELAY_LOCATION]
	}

	public getSort(): string | undefined {
		return this.properties[SORT]
	}

	public getType(): string | undefined {
		return this.properties[TYPE]
	}

	public getUrl(): string | undefined {
		return this.properties[URL]
	}

	public getUser(): string | undefined {
		return this.properties[USER]
	}

	public getVideoQuality(): VideoQuality | undefined {
		return this.properties[VIDEO_QUALITY]
	}

	public getVidStatus(): string | undefined {
		return this.properties[VID_STATUS]
	}

	public hideMMHD(): boolean {
		return this.is(HIDE_MMHD)
	}

	public isActive(): boolean {
		return this.is(ACTIVE)
	}

	public isAppFlag(): boolean {
		return this.is(APP_FLAG)
	}

	public isApproved(): boolean {
		return this.is(APPROVED)
	}

	public isBackgroundImage(): boolean {
		return this.is(BACKGROUND_IMAGE)
	}

	public isFlippin(): boolean {
		return this.is(FLIPPIN)
	}

	public isHidden(): boolean {
		return this.is(HIDDEN)
	}

	public isPowerPoint(): boolean {
		return this.is(POWER_POINT)
	}

	public useAsLogo(): boolean {
		return this.is(USE_AS_LOGO)
	}

	public useDeviceCache(): boolean {
		return this.is(USE_DEVICE_CACHE)
	}
}
