export class HTTPException extends Error {
	constructor(status: number) {
		super()

		this.message = `Network Error (${status})`
	}
}
