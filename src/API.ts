import crypto from 'crypto'
import axios from 'axios'
import { ReadStream } from 'fs'
import { format, addMinutes } from 'date-fns'
import FormData from 'form-data'
import { GenericOperation, Parameters, Config } from './types/API'
import { Record } from './Record'
import { FOLDER_ID, FOLDER_NAME, FOLDER_SHARED, FOLDER_TAGS, FOLDER_TYPE } from './constants/Folder'
const concat = require('concat-stream')

import {
	OBJECT_ID,
	MEDIAREF,
	MEDIAREF_THUMB,
	MEDIAREF_PREVIEW,
	ID,
	NAME,
	TYPE,
	ACTIVE,
	DATE,
	VID_STATUS,
	BACKGROUND_IMAGE,
	ORIENTATION,
	FLIPPIN,
	VIDEO_QUALITY,
	FOLDER_ID as RECORD_FOLDER_ID,
	HIDDEN,
	POWER_POINT,
	USE_AS_LOGO,
	APPROVED,
	USER,
	ATTRIBUTES,
	DESCRIPTION_NOTES,
	DURATION,
	SORT,
	HIDE_MMHD,
	APP_FLAG,
	TYPE_HTML,
	TYPE_VIDEO,
	TYPE_IMAGE
} from './constants/Record'

import {
	OP_ADD_REMOTE,
	OP_EDIT,
	OP_EDIT_REMOTE,
	PARAM_APPROVED,
	PARAM_BACKGROUND_IMAGE,
	PARAM_FLIPPIN,
	PARAM_HIDDEN,
	PARAM_POWER_POINT,
	PARAM_RESET_ACTIVE,
	PARAM_RESET_APP_FLAG,
	PARAM_RESET_DEVICE_CACHE,
	PARAM_SET_ACTIVE,
	PARAM_SET_APP_FLAG,
	PARAM_SET_DEVICE_CACHE,
	PARAM_USE_AS_LOGO,
	PARAM_ACTIVE,
	PARAM_APP_FLAG,
	PARAM_DEVICE_CACHE,
	PARAM_ATTRIBUTES,
	CONFIG_TIMEOUT,
	STATUS_OK,
	OP_ADD,
	OP_LIST_FOLDERS,
	VALID_OPERATIONS,
	OP_LIST,
	OP_LIST_REMOTE,
	OP_VIEW,
	OP_VIEW_REMOTE,
	PARAM_GROUP_OWNER,
	PARAM_MEDIA,
	PARAM_NAME,
	PARAM_OWNER,
	PARAM_TYPE,
	PARAM_URL,
	PARAM_OBJECT_ID,
	OP_DELETE,
	OP_DELETE_REMOTE,
	CACHEABLE_OPERATIONS
} from './constants/API'
import { Folder } from './Folder'
import { RecordType } from './types/Record'
import { HTTPException } from './Exceptions'

export type MediaAPIOptions = {
	accountId: string
	userId: string
	password: string
	timeout?: number
}

export class MediaApi {
	accountId: string
	userId: string
	password: string
	config: Config

	last_response: any
	last_response_raw: any
	cache: any

	constructor({ accountId, userId, password, ...config }: MediaAPIOptions) {
		this.accountId = accountId
		this.userId = userId
		this.password = password
		this.config = { [CONFIG_TIMEOUT]: 10 }
		this.cache = {}

		Object.keys(config).forEach(key => {
			// FIXME:
			//@ts-ignore
			this.config[key] = config[key]
		})
	}

	/**
	 * Executes an API call.
	 *
	 * @param string operation
	 * @param array  parameters
	 *
	 * @return array
	 * @throws Exception\InvalidArgumentException
	 * @throws Exception\CurlException
	 * @throws Exception\HttpException
	 */
	public async exec(operation: GenericOperation, parameters: any) {
		if (!(operation in VALID_OPERATIONS)) {
			throw new Error(`${operation} is not a valid operation`)
		}

		this.last_response = null
		this.last_response_raw = null

		if (this.cache[operation]) {
			return this.cache[operation]
		}

		const response = await this.makeRequest(operation, parameters)

		if (operation in CACHEABLE_OPERATIONS) {
			this.cache[operation] = response
		}

		return response
	}

	buildParameters(operation: GenericOperation, parameters: Parameters): Parameters {
		const params: Parameters = {}

		Object.keys(parameters).forEach(key => {
			//@ts-ignore
			let value = parameters[key]
			let param = key

			switch (key) {
				// If these parameters are falsy, don't include them.
				case PARAM_APPROVED:
				case PARAM_BACKGROUND_IMAGE:
				case PARAM_FLIPPIN:
				case PARAM_HIDDEN:
				case PARAM_POWER_POINT:
				case PARAM_RESET_ACTIVE:
				case PARAM_RESET_APP_FLAG:
				case PARAM_RESET_DEVICE_CACHE:
				case PARAM_SET_ACTIVE:
				case PARAM_SET_APP_FLAG:
				case PARAM_SET_DEVICE_CACHE:
				case PARAM_USE_AS_LOGO:
					if (!value) return
					break

				// The API does not accept the "active" flag for "edit", "editRemote", or
				// "addRemote" operations, so we need to convert it to either "setactive" or
				// "resetactive", depending on its value. If it's any other operation and the
				// value is falsy, don't include the parameter.
				case PARAM_ACTIVE:
					if ([OP_EDIT, OP_EDIT_REMOTE, OP_ADD_REMOTE].includes(operation)) {
						param = !!value ? PARAM_SET_ACTIVE : PARAM_RESET_ACTIVE
						value = true
					} else if (!value) {
						return
					}

					break

				// If this is an "edit" operation, we need to convert the "appFlag" parameter to
				// either "setAppFlag" or "resetAppFlag", depending on its value. If it's any
				// other operation, and its value is falsy, don't include the parameter.
				case PARAM_APP_FLAG:
					if ([OP_EDIT_REMOTE, OP_EDIT].includes(operation)) {
						param = !!value ? PARAM_SET_APP_FLAG : PARAM_RESET_APP_FLAG
						value = true
					}

					break

				// The API does not actually accept the "deviceCache" property, so we need to
				// convert it to either "setdevicecache" or "resetdevicecache", depending on its
				// value.
				case PARAM_DEVICE_CACHE:
					param = !!value ? PARAM_SET_DEVICE_CACHE : PARAM_RESET_DEVICE_CACHE
					value = true
					break

				// If the "attribute" parameter is an array, convert it to a comma-separated
				// string.
				case PARAM_ATTRIBUTES:
					if (Array.isArray(value)) {
						value = value.join(',')
					}

					break

				default:
					break
			}

			// @ts-ignore
			params[param] = value
		})

		return params
	}

	async makeRequest(operation: GenericOperation, parameters: Parameters) {
		const date = new Date()
		const timestamp = format(addMinutes(date, date.getTimezoneOffset()), 'Y/M/d H:mm:ss')

		const signature = crypto
			.createHash('sha512')
			.update(['CCHDMEDIAAPI', this.accountId, this.userId, this.password, timestamp].join('|'))
			.digest('hex')

		let params: Parameters = {
			operation,
			timestamp,
			protoVersion: '1.0',
			userid: this.userId,
			signature,
			...this.buildParameters(operation, parameters)
		}

		// If token authentication is being used, the password (which should be the
		// token) needs to be copied to the "token" parameter.
		if (this.userId === '$token') {
			// @ts-ignore FIXME
			params.token = this.password
		}

		const url = `https://${this.accountId}.channelshd.com/i7:CCHD.rpc_mediaAPI`

		const formData = new FormData()

		Object.keys(params).forEach(key => {
			formData.append(
				key,
				//@ts-ignore
				params[key]
			)
		})

		const getFormDataBuffer = () => {
			return new Promise(resolve => formData.pipe(concat({ encoding: 'buffer' }, resolve)))
		}

		const { data, status } = await axios.post(url, await getFormDataBuffer(), {
			headers: formData.getHeaders(),
			timeout: this.config[CONFIG_TIMEOUT] * 1000
		})

		if (status !== 200) {
			throw new HTTPException(status)
		}

		return this.parseResponse(operation, data)
	}

	parseResponse(operation: GenericOperation, httpResponse: string) {
		const responseParts = httpResponse.split('\n', 2)
		const rawStatus = responseParts[0]

		if (rawStatus !== STATUS_OK) {
			const [, error] = rawStatus.split(':')

			throw new Error(error)
		}

		// TODO: Do we need to trim the \n
		// $body = rtrim( $response_parts[1], "\n" );
		const body = responseParts[1].trim()

		let data = null

		switch (operation) {
			case OP_ADD:
			case OP_EDIT: {
				const [objectId, mediaRef, mediaRefThumb, mediaRefPreview] = body.split(':')

				data = {
					[OBJECT_ID]: objectId,
					[MEDIAREF]: mediaRef,
					[MEDIAREF_THUMB]: mediaRefThumb,
					[MEDIAREF_PREVIEW]: mediaRefPreview
				}

				break
			}
			case OP_ADD_REMOTE:
			case OP_EDIT_REMOTE:
				const [objectId, id] = body.split(':')

				data = { [OBJECT_ID]: objectId, [ID]: id }
				break

			case OP_LIST:
				data = this.parseRecordList(body)
				break

			case OP_LIST_FOLDERS:
				data = this.parseFolderList(body)
				break

			case OP_LIST_REMOTE:
				data = this.parseRemoteRecordList(body)
				break

			case OP_VIEW:
				data = this.parseRecordList(body)[0]
				break

			case OP_VIEW_REMOTE:
				data = this.parseRemoteRecordList(body)[0]
				break
		}

		return data
	}

	parseRecordList(body: any): Record[] {
		const properties = {
			[OBJECT_ID]: true,
			[ID]: true,
			[NAME]: true,
			[TYPE]: true,
			[ACTIVE]: true,
			[DATE]: true,
			[MEDIAREF]: true,
			[MEDIAREF_THUMB]: true,
			[MEDIAREF_PREVIEW]: true,
			[VID_STATUS]: true,
			[BACKGROUND_IMAGE]: true,
			[ORIENTATION]: true,
			[FLIPPIN]: true,
			[VIDEO_QUALITY]: true,
			[RECORD_FOLDER_ID]: true,
			[HIDDEN]: true,
			[POWER_POINT]: true,
			[USE_AS_LOGO]: true,
			[APPROVED]: true,
			[USER]: true,
			[ATTRIBUTES]: true,
			[DESCRIPTION_NOTES]: true,
			[DURATION]: true,
			[SORT]: true,
			[HIDE_MMHD]: true,
			_unknown_flag_1: true,
			_unknown_flag_2: true,
			[APP_FLAG]: true
		}

		const listBody = this.parseListBody(body, properties)

		const records = listBody.map(record => {
			if (record[DATE]) {
				record[DATE] = new Date(record[DATE])
			}

			if (record[ATTRIBUTES]) {
				record[ATTRIBUTES] = record[ATTRIBUTES].split(',')
			}

			return new Record(record)
		})

		return records
	}
	parseFolderList(body: any): Folder[] {
		const properties = {
			[FOLDER_ID]: true,
			[FOLDER_NAME]: true,
			[FOLDER_TAGS]: true,
			[FOLDER_TYPE]: true,
			[FOLDER_SHARED]: true
		}

		const listBody = this.parseListBody(body, properties)

		const folders = listBody.map(folder => new Folder(folder))

		return folders
	}

	parseListBody(body: string, properties: { [k: string]: boolean }): any[] {
		//@ts-ignore
		const rows = []

		body.split('\n').forEach(row => {
			const fields = row.split('\t')

			const entry = {}

			Object.keys(properties).forEach((key, index) => {
				if (!fields[index] || fields[index] === '') {
					return
				}

				//@ts-ignore
				entry[key] = fields[index]
			})

			rows.push(entry)
		})

		//@ts-ignore
		return rows
	}

	parseRemoteRecordList(body: any): Record[] {
		const properties = {
			[OBJECT_ID]: true,
			[ID]: true,
			[NAME]: true,
			[TYPE]: true,
			[ACTIVE]: true,
			[DATE]: true,
			[MEDIAREF]: true,
			[MEDIAREF_THUMB]: true,
			[MEDIAREF_PREVIEW]: true,
			[VID_STATUS]: true,
			[BACKGROUND_IMAGE]: true,
			[ORIENTATION]: true,
			[FLIPPIN]: true,
			[VIDEO_QUALITY]: true,
			[FOLDER_ID]: true,
			[HIDDEN]: true,
			[POWER_POINT]: true,
			[USE_AS_LOGO]: true,
			[APPROVED]: true,
			[USER]: true,
			[ATTRIBUTES]: true,
			[DESCRIPTION_NOTES]: true,
			[DURATION]: true,
			[SORT]: true,
			[HIDE_MMHD]: true,
			_unknown_flag_1: true,
			_unknown_flag_2: true,
			[APP_FLAG]: true
		}

		const listBody = this.parseListBody(body, properties)

		const records = listBody.map(record => {
			if (record[DATE]) {
				record[DATE] = new Date(record[DATE])
			}

			if (record[ATTRIBUTES]) {
				record[ATTRIBUTES] = record[ATTRIBUTES].split(',')
			}

			return new Record(record)
		})

		return records
	}

	public async add(
		file: ReadStream,
		name: string,
		type: RecordType,
		params: any
	): Promise<{ data: Record | undefined; error?: string }> {
		const addParameters = {
			...params,
			[PARAM_ACTIVE]: true,
			[PARAM_GROUP_OWNER]: 'DefaultContent',
			[PARAM_MEDIA]: file,
			[PARAM_NAME]: name,
			[PARAM_OWNER]: this.userId,
			[PARAM_TYPE]: type
		}

		const response = await this.exec(OP_ADD, addParameters)

		if (response.error) {
			throw new Error(response.error)
		}

		return response.data
	}

	// TODO: FIX THE parameters: ANY
	public addHtml(file: ReadStream, name: string, parameters: any) {
		return this.add(file, name, TYPE_HTML, parameters)
	}

	// TODO: FIX THE parameters: ANY
	public addVideo(file: ReadStream, name: string, parameters: any) {
		return this.add(file, name, TYPE_VIDEO, parameters)
	}

	// TODO: FIX THE parameters: ANY
	public addImage(file: ReadStream, name: string, parameters?: any) {
		return this.add(file, name, TYPE_IMAGE, parameters)
	}

	public async addRemote(
		url: string,
		name: string,
		type: RecordType,
		parameters: any
	): Promise<Record | false> {
		const addParameters = {
			...parameters,
			[PARAM_ACTIVE]: true,
			[PARAM_GROUP_OWNER]: 'DefaultContent',
			[PARAM_NAME]: name,
			[PARAM_OWNER]: this.userId,
			[PARAM_TYPE]: type,
			[PARAM_URL]: url
		}

		const response = await this.exec(OP_ADD_REMOTE, addParameters)

		if (response.error) {
			throw new Error(response.error)
		}

		return response.data
	}

	public addRemoteImage(url: string, name: string, parameters: any) {
		return this.addRemote(url, name, TYPE_IMAGE, parameters)
	}

	public addRemoteHtml(url: string, name: string, parameters: any) {
		return this.addRemote(url, name, TYPE_HTML, parameters)
	}

	public async edit(
		objectId: string,
		file: ReadStream,
		type: RecordType,
		parameters: any
	): Promise<Record | false> {
		const editParameters = {
			...parameters,
			[PARAM_MEDIA]: file,
			[PARAM_OBJECT_ID]: objectId,
			[PARAM_TYPE]: type
		}

		const response = await this.exec(OP_EDIT, editParameters)

		if (response.error) {
			throw new Error(response.error)
		}

		return response.data
	}

	public editHtml(objectId: string, file: ReadStream, parameters: any) {
		return this.edit(objectId, file, TYPE_HTML, parameters)
	}

	public editImage(objectId: string, file: ReadStream, parameters: any) {
		return this.edit(objectId, file, TYPE_IMAGE, parameters)
	}

	public editVideo(objectId: string, file: ReadStream, parameters: any) {
		return this.edit(objectId, file, TYPE_VIDEO, parameters)
	}

	public async editRemote(
		objectId: string,
		url: string,
		type: RecordType,
		parameters: any
	): Promise<Record | false> {
		const editParameters = {
			...parameters,
			[PARAM_OBJECT_ID]: objectId,
			[PARAM_TYPE]: type,
			[PARAM_URL]: url
		}

		const response = await this.exec(OP_EDIT_REMOTE, editParameters)

		if (response.error) {
			throw new Error(response.error)
		}

		return response.data
	}

	public editRemoteImage(objectId: string, url: string, parameters: any) {
		return this.editRemote(objectId, url, TYPE_IMAGE, parameters)
	}

	public editRemoteHtml(objectId: string, url: string, parameters: any) {
		return this.editRemote(objectId, url, TYPE_HTML, parameters)
	}

	public async delete(objectId: string): Promise<boolean> {
		await this.exec(OP_DELETE, { [PARAM_OBJECT_ID]: objectId })

		return true
	}

	public async deleteRemote(objectId: string): Promise<boolean> {
		await this.exec(OP_DELETE_REMOTE, {
			[PARAM_OBJECT_ID]: objectId
		})

		return true
	}

	/**
	 * Returns the response from the last API call. If 'raw' is TRUE, returns
	 * the unparsed response string.
	 *
	 * @param bool $raw
	 *
	 * @return array|string
	 */
	public getLastResponse(raw = false): Record | Record[] | string {
		return raw ? this.last_response_raw : this.last_response
	}
}
