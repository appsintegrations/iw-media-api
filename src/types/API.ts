import {
	CONFIG_TIMEOUT,
	PARAM_ACTIVE,
	PARAM_APP_FLAG,
	PARAM_APPROVED,
	PARAM_ATTRIBUTES,
	PARAM_BACKGROUND_IMAGE,
	PARAM_DESCRIPTION_NOTES,
	PARAM_DEVICE_CACHE,
	PARAM_DEVICE_CACHE_INTERVAL,
	PARAM_FLIPPIN,
	PARAM_FOLDER_ID,
	PARAM_GROUP_OWNER,
	PARAM_HIDDEN,
	PARAM_HORIZONTAL_OFFSET,
	PARAM_ID,
	PARAM_LOGO_IMAGE,
	PARAM_MEDIA,
	PARAM_MEDIAREF,
	PARAM_MODULE,
	PARAM_NAME,
	PARAM_OBJECT_ID,
	PARAM_ORIENTATION,
	PARAM_OWNER,
	PARAM_POWER_POINT,
	PARAM_RELAY_LOCATION,
	PARAM_RESET_ACTIVE,
	PARAM_RESET_APP_FLAG,
	PARAM_RESET_DEVICE_CACHE,
	PARAM_SET_ACTIVE,
	PARAM_SET_APP_FLAG,
	PARAM_SET_DEVICE_CACHE,
	PARAM_TYPE,
	PARAM_URL,
	PARAM_USE_AS_LOGO,
	PARAM_USER,
	PARAM_VERTICAL_OFFSET,
	PARAM_VIDEO_QUALITY
} from '../constants/API'

export type GenericOperation =
	| 'add'
	| 'addRemote'
	| 'delete'
	| 'deleteRemote'
	| 'edit'
	| 'editRemote'
	| 'list'
	| 'folderList'
	| 'listRemote'
	| 'view'
	| 'viewRemote'

export type ProtocolVersion = '1.0'

export type Orientation = 'horizontal' | 'vertical'

export type Config = {
	[CONFIG_TIMEOUT]: number
}

export type Parameters = {
	operation?: GenericOperation
	timestamp?: string
	protoVersion?: ProtocolVersion
	userid?: string
	signature?: string

	[PARAM_ACTIVE]?: boolean
	[PARAM_APP_FLAG]?: boolean
	[PARAM_APPROVED]?: boolean
	[PARAM_ATTRIBUTES]?: string
	[PARAM_BACKGROUND_IMAGE]?: boolean
	[PARAM_DESCRIPTION_NOTES]?: string
	[PARAM_DEVICE_CACHE]?: string
	[PARAM_DEVICE_CACHE_INTERVAL]?: string
	[PARAM_FLIPPIN]?: boolean
	[PARAM_FOLDER_ID]?: string
	[PARAM_GROUP_OWNER]?: string
	[PARAM_HIDDEN]?: boolean
	[PARAM_HORIZONTAL_OFFSET]?: string
	[PARAM_ID]?: string
	[PARAM_LOGO_IMAGE]?: string
	[PARAM_MEDIA]?: boolean
	[PARAM_MEDIAREF]?: string
	[PARAM_MODULE]?: string
	[PARAM_NAME]?: string
	[PARAM_OBJECT_ID]?: string
	[PARAM_ORIENTATION]?: Orientation
	[PARAM_OWNER]?: string
	[PARAM_POWER_POINT]?: boolean
	[PARAM_RELAY_LOCATION]?: string
	[PARAM_RESET_ACTIVE]?: boolean
	[PARAM_RESET_APP_FLAG]?: boolean
	[PARAM_RESET_DEVICE_CACHE]?: boolean
	[PARAM_SET_ACTIVE]?: boolean
	[PARAM_SET_APP_FLAG]?: boolean
	[PARAM_SET_DEVICE_CACHE]?: boolean
	[PARAM_TYPE]?: string
	[PARAM_URL]?: string
	[PARAM_USE_AS_LOGO]?: boolean
	[PARAM_USER]?: string
	[PARAM_VERTICAL_OFFSET]?: string
	[PARAM_VIDEO_QUALITY]?: string
}
