import {
	FOLDER_ID,
	FOLDER_NAME,
	FOLDER_TYPE,
	FOLDER_TAGS,
	FOLDER_SHARED
} from '../constants/Folder'

export type FolderProperties = {
	[FOLDER_ID]: number
	[FOLDER_NAME]: string
	[FOLDER_TYPE]: string
	[FOLDER_TAGS]: string
	[FOLDER_SHARED]: string
}
