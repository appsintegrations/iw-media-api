import {
	APP_FLAG,
	ACTIVE,
	APPROVED,
	ATTRIBUTES,
	BACKGROUND_IMAGE,
	DATE,
	DESCRIPTION_NOTES,
	DEVICE_CACHE_INTERVAL,
	DURATION,
	FLIPPIN,
	FOLDER_ID,
	HIDDEN,
	HIDE_MMHD,
	ID,
	MEDIAREF,
	MEDIAREF_THUMB,
	MEDIAREF_PREVIEW,
	MODULE,
	NAME,
	OBJECT_ID,
	ORIENTATION,
	POWER_POINT,
	RELAY_LOCATION,
	SORT,
	TYPE,
	USE_AS_LOGO,
	USE_DEVICE_CACHE,
	USER,
	VID_STATUS,
	VIDEO_QUALITY,
	URL,
	TYPE_IMAGE,
	EXTENSION_TYPES,
} from '../constants/Record'

export type VideoQuality = 1 | 2 | 3 | 4 | 5

export type Property =
	| 'active'
	| 'appFlag'
	| 'approved'
	| 'attribute'
	| 'backgroundimage'
	| 'Date'
	| 'descriptionNotes'
	| 'deviceCacheInterval'
	| 'duration'
	| 'flippin'
	| 'folderID'
	| 'hidden'
	| 'hideMMHD'
	| 'id'
	| 'mediaref'
	| 'mediarefThumb'
	| 'mediarefPreview'
	| 'module'
	| 'name'
	| 'objectid'
	| 'orientation'
	| 'powerPoint'
	| 'relayLocation'
	| 'sort'
	| 'type'
	| 'url'
	| 'useAsLogo'
	| 'deviceCache'
	| 'User'
	| 'vidStatus'
	| 'videoQuality'

export type Properties = {
	[ACTIVE]?: string
	[APP_FLAG]?: string
	[APPROVED]?: string
	[ATTRIBUTES]?: string[]
	[BACKGROUND_IMAGE]?: string
	[DATE]?: Date
	[DESCRIPTION_NOTES]?: string
	[DEVICE_CACHE_INTERVAL]?: string
	[DURATION]?: string
	[FLIPPIN]?: string
	[FOLDER_ID]?: number
	[HIDDEN]?: string
	[HIDE_MMHD]?: string
	[ID]?: string
	[MEDIAREF]?: string
	[MEDIAREF_THUMB]?: string
	[MEDIAREF_PREVIEW]?: string
	[MODULE]?: string
	[NAME]?: string
	[OBJECT_ID]?: string
	[ORIENTATION]?: string
	[POWER_POINT]?: string
	[RELAY_LOCATION]?: string
	[SORT]?: string
	[TYPE]?: string
	[URL]?: string
	[USE_AS_LOGO]?: string
	[USE_DEVICE_CACHE]?: string
	[USER]?: string
	[VID_STATUS]?: string
	[VIDEO_QUALITY]?: VideoQuality
}

export type RecordType = 'HTML' | 'Video' | 'Image'
